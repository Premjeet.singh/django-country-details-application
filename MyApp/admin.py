from django.contrib import admin
# Register your models here.
from .models import Country, Contact, Comment,newRegistration,newSignupform

admin.site.register(Country)
admin.site.register(Comment)
admin.site.register(Contact)
admin.site.register(newRegistration)
admin.site.register(newSignupform)
