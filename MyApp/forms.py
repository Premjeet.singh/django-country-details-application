from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.http import HttpResponse
from django.template.loader import render_to_string

from .models import Country, Comment,newRegistration,newSignupform
from datetime import datetime,timedelta
from django.core.mail import EmailMessage
from django.conf import settings
from .utility import send_attachment
from .utils import send

#
class SignupUser(UserCreationForm):
    first_name = forms.CharField(max_length=100, required=True)
    last_name = forms.CharField(max_length=100, required=True)
    email = forms.EmailField(max_length=250)
    start_date = forms.DateField(help_text='Required. Format: YYYY-MM-DD')
    end_date = forms.DateField(help_text='Required. Format: YYYY-MM-DD')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'start_date','end_date','password1', 'password2',)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("email is taken")
        return email

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        qs = User.objects.filter(first_name=first_name)
        if qs.exists():
            raise forms.ValidationError("first_name is taken")
        return first_name


#
class unique():
    class Meta:
        model = newRegistration
        fields = '__all__'

class newSignupform():
    class Meta:
        model = newSignupform
        fields = '__all__'




class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            date_joining= user.date_joined
            date_joining+= timedelta(days=1)
            if not user:
                raise forms.ValidationError('This user does not exist')
            if user.is_active and user.last_login > date_joining:
                send_attachment()
                # send()
                # subject = "Regarding subscription"
                # msg = "your subscription is expired,renew it"
                # to = "spremjeet3@gmail.com"
                # html_template = "subscription.html"
                # html_message = render_to_string(html_template)
                # message = EmailMessage(subject, html_message, settings.EMAIL_HOST_USER, [to])
                # message.content_subtype = 'html'
                # message.send()
                # return HttpResponse('success')
                raise forms.ValidationError("subscription expired")
            if not user.check_password(password):
                raise forms.ValidationError('Incorrect password')
            if not user.is_active:
                raise forms.ValidationError('This user is not active')
        return super(UserLoginForm, self).clean(*args, **kwargs)

class CountryForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = '__all__'


# for comment
class CommentForm(forms.ModelForm):
    content = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'form-control',
        'placeholder': 'Type your comment',
        'id': 'usercomment',
        'rows': '4'
    }))

    class Meta:
        model = Comment
        fields = ('content',)


class EmailForm(forms.Form):
    email = forms.EmailField()
    subject = forms.CharField(max_length=100)
    attach = forms.Field(widget=forms.FileInput)
    message = forms.CharField(widget=forms.Textarea)