from django.contrib.auth.models import User
from django.db import models
from django.forms import ModelForm, Textarea

# Create your models here.
from django.urls import reverse



class Country(models.Model):
    image = models.ImageField(upload_to='myapp/')
    country_name = models.CharField(max_length=200)

    date = models.DateField(max_length=50)
    native_name = models.CharField(max_length=50)
    capital = models.CharField(max_length=20)
    population = models.IntegerField()
    region = models.CharField(max_length=20)
    sub_region = models.CharField(max_length=20)
    area_km = models.IntegerField()
    country_code = models.IntegerField()
    Languages = models.CharField(max_length=20)
    currency = models.CharField(max_length=20)
    timezones = models.TimeField(auto_now_add=True)

    def __str__(self):
        return self.country_name

    def get_absolute_url(self):
        return reverse('detail-view', kwargs={
            'id': self.id
        })

    @property
    def get_comments(self):
        return self.comments.all().order_by('-timestamp')


class Meta:
    db_table = "MyApp"


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    post = models.ForeignKey('Country', related_name='comments', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class Contact(models.Model):
    sno = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=13)
    email = models.CharField(max_length=100)
    content = models.TextField()
    timeStamp = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return "Message from " + self.name + ' - ' + self.email


# class registration(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     start_date = models.DateTimeField(auto_now_add=True)
#     end_date = models.DateTimeField(auto_now_add=True)

class newRegistration(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    start_date= models.DateField()
    end_date = models.DateField()

class newSignupform(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    start_date= models.DateField()
    end_date = models.DateField()

