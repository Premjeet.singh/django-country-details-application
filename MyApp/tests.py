from django.urls import reverse, resolve

from .forms import CountryForm
from .models import Country
from .views import detail

from django.contrib.auth.models import User
from django.test import TestCase


class ListViewTests(TestCase):
    def setUp(self):
        self.board = Country.objects.create(
            image='A.png',
            country_name='Django',
            date='2020-04-6',
            native_name='Django board.',
            capital='Django board.',
            population='100',
            region='Django board.',
            sub_region='Django board.',
            area_km='2222',
            country_code='91',
            Languages='Django board.',
            currency='Django board.',
            timezones='Django board.',
        )
        url = reverse('list-view')
        self.response = self.client.get(url)

    def test_list_view_status_code(self):
        self.assertEquals(self.response.status_code, 200)


class DetailsViewTests(TestCase):
    def setUp(self):
        Country.objects.create(
            image='A.png',
            country_name='Django',
            date='2020-04-6',
            native_name='Django board.',
            capital='Django board.',
            population='100',
            region='Django board.',
            sub_region='Django board.',
            area_km='2222',
            country_code='91',
            Languages='Django board.',
            currency='Django board.',
            timezones='Django board.',
        )

    def test_detail_view_success_status_code(self):
        url = reverse('detail-view', kwargs={'id': 1})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_detail_view_not_found_status_code(self):
        url = reverse('detail-view', kwargs={'id': 99})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_country_url_resolves_country_detail_view(self):
        view = resolve('/detail/1/')
        self.assertEquals(view.func, detail)


class CreateViewTests(TestCase):
    def setUp(self):
        Country.objects.create(
            image='A.png',
            country_name='Django',
            date='2020-04-6',
            native_name='Django board.',
            capital='Django board.',
            population='100',
            region='Django board.',
            sub_region='Django board.',
            area_km='2222',
            country_code='91',
            Languages='Django board.',
            currency='Django board.',
            timezones='Django board.',

        )
        User.objects.create_user(username='john', email='john@gmail.com', password='123')

    def test_new_country_view_success_status_code(self):
        url = reverse('list-view')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_csrf(self):
        url = reverse('create-view')
        response = self.client.get(url)
        self.assertContains(response, 'csrfmiddlewaretoken')

    #
    def test_contains_form(self):
        url = reverse('create-view')
        response = self.client.get(url)
        form = response.context.get('form')
        self.assertIsInstance(form, CountryForm)

    def test_new_country_valid_post_data(self):
        url = reverse('create-view')
        data = {
            'image': 'A.png',
            'country_name': 'Django',
            'date': '2020-04-6',
            'native_name': 'Django board.',
            'capital': 'Django board.',
            'population': '100',
            'region': 'Django board.',
            'sub_region': 'Django board.',
            'area_km': '2222',
            'country_code': '91',
            'Languages': 'Django board.',
            'currency': 'Django board.',
            'timezones': 'Django board.',
        }
        response = self.client.post(url, data)
        self.assertTrue(Country.objects.exists())

    def test_new_country_invalid_post_data_empty_fields(self):
        url = reverse('create-view')
        data = {
            'image': '',
            'country_name': '',
            'date': '',
            'native_name': '',
            'capital': '',
            'population': '',
            'region': '',
            'sub_region': '',
            'area_km': '',
            'country_code': '',
            'Languages': '',
            'currency': '',
            'timezones': '',
        }
        response = self.client.post(url, data)
        self.assertEquals(response.status_code, 200)
        self.assertTrue(Country.objects.exists())
