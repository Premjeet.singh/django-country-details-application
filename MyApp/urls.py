from django.conf.urls import url
from django.urls import include, path
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views
from .import views


from rest_framework import routers

from .views import EmailAttachementView

router = routers.DefaultRouter()
router.register(r'MyApp', views.CountryViewSet)
router.register(r'comment', views.CommentViewSet)
router.register(r'contact', views.ContactViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('', TemplateView.as_view(template_name='dashboard.html'), name='home1'),
    # path('home/', views.Homepage, name='home'),
    path('social-auth/', include('social_django.urls', namespace='social')),
    path('home/', views.contact1, name="contact1"),
    path('signup/', views.signupform, name='signup'),
    path('login/', views.user_login, name='login'),
    path('contact/', views.contact, name='contact'),
    path('logout/', views.user_logout, name='logout'),
    path('dashboard/', views.dashboard, name="dashboard"),

    path('create_view/', views.create_view, name='create-view'),
    path('newSignup/', views.newSignup, name='newSignup'),

    path('list_view/', views.list_view, name="list-view"),
    path('detail/<id>/', views.detail, name='detail-view'),
    path('delete/<int:id>', views.delete),
    path('edit/<int:pk>/', views.edit, name="edit-view"),
    path('search_view/', views.search_view, name="search-view"),

    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    path('email/', EmailAttachementView.as_view(), name='emailattachment')
]
