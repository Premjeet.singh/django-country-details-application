# from django.core.mail import EmailMessage
from email.message import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings
import imghdr
import smtplib
import os


def send():
    # subject = "Regarding subscription"
    # msg = "your subscription is expired,renew it"
    # to = "spremjeet3@gmail.com"
    # # html_template = "subscription.html"
    # # html_message = render_to_string(html_template)
    # message = EmailMessage(subject,msg, settings.EMAIL_HOST_USER, [to])
    # # message.content_subtype = 'html'
    # files=[A.png,ind.png]
    # with open('A.png', 'rb')as attach_file:
    #     image_name = attach_file.name
    #     image_type = imghdr.what(attach_file.name)
    #     image_data = attach_file.read()
    # message.attach(image_data, maintype="image", subtype=image_type, filename=image_name)
    # for f in files:
    #     message.attach( f.read(), f.content_type)
    #     message.send()
    # message.send()
    email_id=os.environ.get('EMAIL_HOST_USER')
    email_pass=os.environ.get('EMAIL_HOST_PASSWORD')
    message=EmailMessage()
    message['subject']='subscriptions'
    message['from']='spremjeet3@gmail.com'
    message['to']='spremjeet3@gmail.com'
    message.set_content='please renew your subscription'
    html_message=open('templates/subscription.html').read()
    message.add_alternative(html_message,subtype='html')

    with open('A.png','rb')as attach_file:
        image_name=attach_file.name
        image_type=imghdr.what(attach_file.name)
        image_data=attach_file.read()
    message.add_attachment(image_data,maintype="image",subtype=image_type,filename=image_name)
    # message.send()
    with smtplib.SMTP_SSL('smtp.gmail.com',465)as smtp:
        smtp.login(email_id,email_pass)
        smtp.send_message(message)