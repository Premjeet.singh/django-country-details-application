from datetime import date, timedelta

from django.conf import settings
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views import View

from .forms import UserLoginForm, SignupUser, CommentForm, EmailForm,unique,newSignupform
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import CountryForm
from .models import Country, Comment
from .models import Contact
from django.contrib import messages

from .serializers import CountrySerializer, CommentSerializer, ContactSerializer, UserSerializer, GroupSerializer
from datetime import datetime,timedelta
from django.template.loader import render_to_string


def user_login(request):
    if request.method == 'POST':
        loginForm = UserLoginForm(request.POST or None)
        if loginForm.is_valid():
            username = loginForm.cleaned_data.get('username')
            password = loginForm.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            date_joining = user.date_joined
            date_joining+= timedelta(days=1)
            if user.is_active and user.last_login < date_joining:
                login(request, user)
                return redirect(contact1)
            else:
                # messages.error(request, 'subscription expired')
                print(hi)
                subject = "Regarding subscription"
                msg = "your subscription is expired,renew it"
                to = "spremjeet3@gmail.com"
                html_template = "subscription.html"
                html_message = render_to_string(html_template)
                message = EmailMessage(subject, html_message, settings.EMAIL_HOST_USER, [to])
                message.content_subtype = 'html'
                message.send()
                return HttpResponse('success')





    else:
        loginForm = UserLoginForm()
    return render(request, 'login.html', {'loginForm': loginForm})


def signupform(request):
    if request.method == 'POST':
        UserRegister = SignupUser(request.POST or None)
        if UserRegister.is_valid():
            UserRegister.save()
            username = UserRegister.cleaned_data.get('username')
            password = UserRegister.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request,user)
            return redirect(user_login)
    else:
        UserRegister = SignupUser()
    return render(request, 'signup.html', {'UserRegister': UserRegister})

def newSignup(request):
    if request.method == "POST":
        form = newSignupform(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect(user_login)
    form = newSignupform()
    print(form)
    return render(request, 'newSignup.html', {"form": form})

# def signupform(request):
#     if request.method == 'POST':
#         UserRegister = unique(request.POST or None)
#         if UserRegister.is_valid():
#             UserRegister.save()
#             username = UserRegister.cleaned_data.get('username')
#             password = UserRegister.cleaned_data.get('password1')
#             user = authenticate(username=username, password=password)
#             login(request,user)
#             return redirect(user_login)
#     else:
#         UserRegister = unique()
#     return render(request, 'signup.html', {'UserRegister': UserRegister})



@login_required
def user_logout(request):
    logout(request)
    return redirect(user_login)


def dashboard(request):
    return render(request, 'dashboard.html')


@login_required
def contact(request):
    if request.method == "POST":
        name = request.POST['name']
        email = request.POST['email']
        phone = request.POST['phone']
        content = request.POST['content']
        # contact = Contact(name=name, email=email, phone=phone, content=content)
        # contact.save()
        if len(name) < 2 or len(email) < 3 or len(phone) < 10 or len(content) < 4:
            messages.error(request, "Please fill the form correctly")
        else:
            contact = Contact(name=name, email=email, phone=phone, content=content)
            contact.save()
            messages.success(request, "Your message has been successfully sent")
    return render(request, 'contact.html')


# list view 1
@login_required
def contact1(request):
    country = Country.objects.all()
    context = {
        "country": country
    }
    return render(request, 'home.html', context)


# list view 0
@login_required
def list_view(request):
    country = Country.objects.all()
    return render(request, 'list_view.html', {'country': country})


# details view
@login_required
def detail(request, id):
    # if user_login:
    country_dt = Country.objects.all()
    country = get_object_or_404(Country, id=id)

    # for commment
    form = CommentForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.instance.user = request.user
            form.instance.post = country
            form.save()
            return redirect(reverse("detail-view", kwargs={
                'id': country.pk
            }))

    context = {
        "country": country,
        "country_dt": country_dt,
        'form': form,
    }
    return render(request, 'detail.html', context)


# user_login(request)


@login_required
def create_view(request):
    if request.method == "POST":
        form = CountryForm(request.POST or None, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('list-view')
    form = CountryForm()
    print(form)
    return render(request, 'create_view.html', {"form": form})


@login_required
def edit(request, pk):
    country = get_object_or_404(Country, pk=pk)
    form = CountryForm(request.POST or None, instance=country)
    if form.is_valid():
        form.save()
        return redirect('/list_view')
    return render(request, 'edit.html', {"form": form})

@login_required
def delete(request, id):
    country = Country.objects.get(id=id)
    country.delete()
    return redirect("/list_view")

@login_required
def search_view(request):
    country_list = Country.objects.all()
    query = request.GET.get('query')
    if query:
        country_list = country_list.filter(
            Q(country_name__icontains=query) |
            Q(capital__icontains=query)
        ).distinct()

    context = {
        "country_list": country_list,
        "query": query,
    }
    return render(request, 'search.html', context)


# rest api

from django.contrib.auth.models import User, Group
from django.db.models import Q
from rest_framework import viewsets, permissions
from rest_framework import filters



class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [filters.SearchFilter]
    search_fields = ['country_name', 'capital']

    def get_queryset(self, *args, **kwargs):
        queryset_list = Country.objects.all()
        query = self.request.GET.get("query")
        if query:
            queryset_list = queryset_list.filter(
                Q(country_name__icontains=query) |
                Q(capital__icontains=query)
            ).distinct()
        return queryset_list


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated]


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAdminUser]


# # for email with attachment:
class EmailAttachementView(View):
    form_class = EmailForm
    template_name = 'emailattachment.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'email_form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            email = form.cleaned_data['email']
            files = request.FILES.getlist('attach')
            mail = EmailMessage(subject, message, settings.EMAIL_HOST_USER, [email])
            for f in files:
                mail.attach(f.name, f.read(), f.content_type)
                mail.send()
                return render(request, self.template_name,
                              {'email_form': form, 'error_message': 'Sent email to %s' % email})
        return render(request, self.template_name,
                      {'email_form': form, 'error_message': 'Unable to send email. Please try again later'})
