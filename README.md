### What about project ?

This is a web based application of Django where we can see country details,update records,delete and can search countries
by its name and its capital name and also can comment on it.
we have developed it using the CRUD operation and user  can login,signup .
And here the some user can comment which is logged by social authentication or authenticated by Django custom login/authentication.


### Project settings and configurations :

1. How do I run this project locally?
    1. Clone the repository:

2. $ cd MyProject/

3. Create a virtual environment to install dependencies in and activate it:
    1. $ virtualenv venv
    2. $ source env/bin/activate
    3.Then install the dependencies:
   (venv)$ pip install -r requirements.txt Note : the (env) in front of the prompt. This indicates that this terminal
   session operates in a virtual environment set up by virtualenv2.
   4. Once pip has finished downloading the dependencies:
    (venv)$ cd project
    (venv)$ python manage.py migrate
    (venv)$ python manage.py runserver

4. Run migrations:
    1. $ python manage.py migrate

5 Create a user:
$ python manage.py createsuperuser

6. Run the server:
   $ python manage.py runserver
   

7. And open 127.0.0.1:8000/dashboard/ in your web browser.

 
8. These are url which you follow:
   And navigate to :

            And navigate to http://127.0.0.1:8000/dashboard/
            
            And navigate to http://127.0.0.1:8000/home/
    
            http://127.0.0.1:8000/create_view/

            And navigate to http://127.0.0.1:8000/list_view/

            And navigate to http://127.0.0.1:8000/contact/

            And navigate to http://127.0.0.1:8000/login/

            And navigate to http://127.0.0.1:8000/singup/