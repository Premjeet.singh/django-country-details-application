from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings


def send():
    subject = "Regarding subscription"
    msg = "your subscription is expired,renew it"
    to = "spremjeet3@gmail.com"
    html_template = "subscription.html"
    html_message = render_to_string(html_template)
    message = EmailMessage(subject, html_message, settings.EMAIL_HOST_USER, [to])
    message.content_subtype = 'html'
    message.send()
    # return HttpResponse('success')
